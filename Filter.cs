﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace face_recognition
{
    /// <summary>
    /// Provides functions related to filtering of skin color for a bitmap image.
    /// </summary>
    public class Filter
    {
        public YCbCrColor Min { get { return new YCbCrColor(Ymin, Cbmin, Crmin); } }
        public YCbCrColor Max { get { return new YCbCrColor(Ymax, Cbmax, Crmax); } }

        public float Ymin { get; set; }
        public float Ymax { get; set; }
        public float Cbmin { get; set; }
        public float Cbmax { get; set; }
        public float Crmin { get; set; }
        public float Crmax { get; set; }

        public Filter()
        {
            Ymin = 0;
            Ymax = 1;
            Cbmin = -.15f;
            Cbmax = .05f;
            Crmin = .05f;
            Crmax = .2f;
            // for darker skin, 0 - .15 and -.15 - 0
        }

        /// <summary>
        /// filters the bitmaps pixels. pixels that fall within the skin tone threshold
        /// are turned white, those that do not are turned black.
        /// </summary>
        /// <param name="baseImage">The image to be filtered</param>
        /// <returns>a new filtered Bitmap</returns>
        public Bitmap filterBitmap(Bitmap baseImage)
        {
            Bitmap filteredBitmap = new Bitmap(baseImage);

            for (int row = 0; row < baseImage.Height; row++)
            {
                for (int col = 0; col < baseImage.Width; col++)
                {
                    YCbCrColor color = YCbCrColor.fromRgb(baseImage.GetPixel(col, row));
                    if (color.Y >= Min.Y && color.Y <= Max.Y
                     && color.Cb >= Min.Cb && color.Cb <= Max.Cb
                     && color.Cr >= Min.Cr && color.Cr <= Max.Cr)
                        filteredBitmap.SetPixel(col, row, Color.White);
                    else
                        filteredBitmap.SetPixel(col, row, Color.Black);
                }
            }

            return filteredBitmap;
        }

        /// <summary>
        /// counts the number of white pixels in the given bitmap image  
        /// </summary>
        /// <param name="image">the image to be counted</param>
        /// <returns>number of white pixels in the image</returns>
        public int numberOfWhitePixels(Bitmap image)
        {
            int white = Color.White.ToArgb();
            int numberOfWhitePixels = 0;

            for (int row = 0; row < image.Height; row++)
            {
                for (int col = 0; col < image.Width; col++)
                {
                    if (image.GetPixel(col, row).ToArgb().Equals(Color.White.ToArgb()))
                        numberOfWhitePixels++;
                }
            }
            return numberOfWhitePixels;
        }

    }
}
