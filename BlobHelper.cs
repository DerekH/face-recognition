﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace face_recognition
{
    /// <summary>
    /// This class provides some utility functions to help out with analyzing blobs
    /// of pixels within Bitmaps.
    /// </summary>
    class BlobHelper
    {
        /// <summary>
        /// returns the relative size of a blob. It does not return the exact size, rather it returns
        /// a ratio of the size. Instead of counting every pixel, it counts approximately every
        /// 25th pixel. It takes a point anywhere inside the blob as a parameter.
        /// </summary>
        /// <param name="x">an x value located inside the blob</param>
        /// <param name="y">a y value located inside the blob</param>
        /// <param name="bitmap">The Bitmap image containing the blob</param>
        /// <returns>the relative blob size</returns>
        public static int blobSize(int x, int y, Bitmap bitmap, int recurseDistance)
        {
            int color = bitmap.GetPixel(x, y).ToArgb();

            if (y + recurseDistance >= bitmap.Height || x + recurseDistance >= bitmap.Width || x - recurseDistance < 0 || y - recurseDistance < 0)
                return 0;

            bitmap.SetPixel(x, y, Color.Red);
            int pixelCount = 1;

            if (bitmap.GetPixel(x, y + recurseDistance).ToArgb().Equals(color))
                pixelCount += blobSize(x, y + recurseDistance, bitmap, recurseDistance);
            if (bitmap.GetPixel(x, y - recurseDistance).ToArgb().Equals(color))
                pixelCount += blobSize(x, y - recurseDistance, bitmap, recurseDistance);
            if (bitmap.GetPixel(x + recurseDistance, y).ToArgb().Equals(color))
                pixelCount += blobSize(x + recurseDistance, y, bitmap, recurseDistance);
            if (bitmap.GetPixel(x - recurseDistance, y).ToArgb().Equals(color))
                pixelCount += blobSize(x - recurseDistance, y, bitmap, recurseDistance);
            return pixelCount;
        }

        public static void colorBlob(int x, int y, Bitmap bitmap, Color start_color, Color end_color)
        {
            if (y >= bitmap.Height || x >= bitmap.Width || x < 0 || y < 0)
                return;

            bitmap.SetPixel(x, y, end_color);

            if (bitmap.GetPixel(x, y + 1).ToArgb().Equals(start_color.ToArgb()))
                colorBlob(x, y + 1, bitmap, start_color, end_color);
            if (bitmap.GetPixel(x, y - 1).ToArgb().Equals(start_color.ToArgb()))
                colorBlob(x, y - 1, bitmap, start_color, end_color);
            if (bitmap.GetPixel(x + 1, y).ToArgb().Equals(start_color.ToArgb()))
                colorBlob(x + 1, y, bitmap, start_color, end_color);
            if (bitmap.GetPixel(x - 1, y).ToArgb().Equals(start_color.ToArgb()))
                colorBlob(x - 1, y, bitmap, start_color, end_color);
            return;
        }

        /// <summary>
        /// finds the furthest point in the direction specified, for the given blob.
        /// </summary>
        /// <param name="bitmap">The Bitmap containing the blob</param>
        /// <param name="location">any Point inside the blob</param>
        /// <param name="direction">the direction to find the furthest point</param>
        /// <returns>the furthest Point in the specified direction</returns>
        public static Point findFurthestPointInDirection(Bitmap bitmap, Point location, string direction)
        {
            BlobInformation mBlobInformation = new BlobInformation(bitmap, location);

            switch (direction)
            {
                case "up":
                    return mBlobInformation.FurthestPixelUp;
                case "down":
                    return mBlobInformation.FurthestPixelDown;
                case "right":
                    return mBlobInformation.FurthestPixelRight;
                case "left":
                    return mBlobInformation.FurthestPixelLeft;
                default:
                    return new Point(0,0); 
            }
        }
    }

    /// <summary>
    /// designed to provide specific information about a specific blob
    /// </summary>
    class BlobInformation
    {
        private Point pointInBlob;
        private Bitmap imageToBeProcessed;
        private int white = Color.White.ToArgb();
        private int recurseDistance = 3;
        private Point furthestPixelLeft = new Point(999999, 0);
        private Point furthestPixelRight = new Point(-1, 0);
        private Point furthestPixelUp = new Point(0, 999999);
        private Point furthestPixelDown = new Point(0, -1);

        public Point FurthestPixelLeft { get { return furthestPixelLeft; } }
        public Point FurthestPixelRight { get { return furthestPixelRight; } }
        public Point FurthestPixelUp { get { return furthestPixelUp; } }
        public Point FurthestPixelDown { get { return furthestPixelDown; } }

        public BlobInformation(Bitmap bitmap, Point point)
        {
            pointInBlob = point;
            imageToBeProcessed = new Bitmap(bitmap);
            findFurthestPointsInAllDirections(pointInBlob.X, pointInBlob.Y);
        }

        /// <summary>
        /// given a point in the blob, it will calculate the furthest pixels in
        /// every direction. They can then be accessed by the caller.
        /// </summary>
        /// <param name="x">x value in the blob</param>
        /// <param name="y">y value in the blob</param>
        private void findFurthestPointsInAllDirections(int x, int y) 
        {
            if (y + recurseDistance >= imageToBeProcessed.Height || x + recurseDistance >= imageToBeProcessed.Width || x - recurseDistance < 0 || y - recurseDistance < 0)
                return;

            if (y < furthestPixelUp.Y)
                furthestPixelUp = new Point(x, y);

            if (y > furthestPixelDown.Y)
                furthestPixelDown = new Point(x, y);

            if (x < furthestPixelLeft.X)
                furthestPixelLeft = new Point(x, y);

            if (x > furthestPixelRight.X)
                furthestPixelRight = new Point(x, y);

            imageToBeProcessed.SetPixel(x, y, Color.Red);

            if (imageToBeProcessed.GetPixel(x, y + recurseDistance).ToArgb().Equals(white))
                findFurthestPointsInAllDirections(x, y + recurseDistance);
            if (imageToBeProcessed.GetPixel(x, y - recurseDistance).ToArgb().Equals(white))
                findFurthestPointsInAllDirections(x, y - recurseDistance);
            if (imageToBeProcessed.GetPixel(x + recurseDistance, y).ToArgb().Equals(white))
                findFurthestPointsInAllDirections(x + recurseDistance, y);
            if (imageToBeProcessed.GetPixel(x - recurseDistance, y).ToArgb().Equals(white))
                findFurthestPointsInAllDirections(x - recurseDistance, y);
            return;
        }
    }
}
