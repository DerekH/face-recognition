﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace face_recognition
{
    public partial class mainForm : Form
    {
        private const int ENTER_KEY = 13;
        private const float STANDARD_IMAGE_HEIGHT = 350;

        private BackgroundWorker imageProcessingThread = new BackgroundWorker();
        private string fileName;
        private Bitmap baseImage;
        private Bitmap filteredImage;
        private Bitmap erodedImage;
        private Bitmap erodedImage2;
        private Bitmap dilatedImage;
        private Bitmap biggestBlobImage;
        private Bitmap faceDimensionsImage;
        private Bitmap faceLocationImage;
        private Bitmap finalImage;

        private Bitmap onlyFace;
        private Bitmap grayOnlyFace;
        private Bitmap inverseFace;
        private Bitmap filteredEyeImage;
        private Bitmap erodedEyeImage;
        private Bitmap dilatedEyeImage;
        private Bitmap narrowedEyeDetection;
        private Bitmap finalEyeImage;

        private Filter mFilter = new Filter();

        private int black = Color.Black.ToArgb();
        private int white = Color.White.ToArgb();
        private int green = Color.Green.ToArgb();

        public mainForm()
        {
            InitializeComponent();

            // Set Up BackgroundWorker Thread
            imageProcessingThread.WorkerSupportsCancellation = true;
            imageProcessingThread.WorkerReportsProgress = true;
            imageProcessingThread.DoWork += new DoWorkEventHandler(runFacialRecognitionAlgorithm);
            imageProcessingThread.ProgressChanged += new ProgressChangedEventHandler(ProgressChanged);
            imageProcessingThread.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AlgorithmIsDoneRunning);

            // Initialize Sliders
            updateSliders();
        }

        ///////////////////////////////////////////////////////////////////////
        ////////  User Interface Functions/Handlers ///////////////////////////
        ///////////////////////////////////////////////////////////////////////
        
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            buttonCancel.Enabled = false;
            progressBar.Value = 0;
            this.imageProcessingThread.CancelAsync();
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            if (textBoxFileName.Text.Length <= 0)
            {
                MessageBox.Show("choose a picture first!");
                return;
            }
            fileName = textBoxFileName.Text;

            if (imageProcessingThread.IsBusy != true)
            {
                imageProcessingThread.RunWorkerAsync();
            }

            buttonCancel.Enabled = true;
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog mOpenFileDialog = new OpenFileDialog();
            mOpenFileDialog.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (mOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                uploadPhoto(mOpenFileDialog.FileName);
            }
        }

        private void buttonUpload_Click(object sender, EventArgs e)
        {
            if(textBoxFileName.Text.Length > 1)
                uploadPhoto(textBoxFileName.Text);
        }

        private void textBoxFileName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ENTER_KEY)
            {
                if (textBoxFileName.Text.Length > 1)
                    uploadPhoto(textBoxFileName.Text);
            }
        }

        private void colorScrollbar_ValueChanged(object sender, EventArgs e)
        {
            TrackBar trackbar = ((TrackBar)sender);
            float scaleFactor = trackBarCbMax.Maximum / 2;

            if (trackbar.Name.Contains("CbMin"))
                mFilter.Cbmin = (float)(trackbar.Value - scaleFactor) / scaleFactor;
            else if (trackbar.Name.Contains("CbMax"))
                mFilter.Cbmax = (float)(trackbar.Value - scaleFactor) / scaleFactor;
            else if (trackbar.Name.Contains("CrMin"))
                mFilter.Crmin = (float)(trackbar.Value - scaleFactor) / scaleFactor;
            else if (trackbar.Name.Contains("CrMax"))
                mFilter.Crmax = (float)(trackbar.Value - scaleFactor) / scaleFactor;
            else if (trackbar.Name.Contains("YMin"))
                mFilter.Ymin = (float)trackbar.Value / scaleFactor;
            else if (trackbar.Name.Contains("YMax"))
                mFilter.Ymax = (float)trackbar.Value / scaleFactor;

            updateSliders();
        }

        /// <summary>
        /// updates the value of the sliders on the UI, as well as their labels
        /// to reflect the current values of mFilter 
        /// </summary>
        private void updateSliders()
        {
            float scaleFactor = trackBarCbMax.Maximum / 2;

            trackBarCbMin.Value = (int)(((float)(mFilter.Cbmin * scaleFactor)) + scaleFactor);
            labelCbMin.Text = "Cb Min: " + mFilter.Cbmin;
            trackBarCbMax.Value = (int)(((float)(mFilter.Cbmax * scaleFactor)) + scaleFactor);
            labelCbMax.Text = "Cb Max: " + mFilter.Cbmax;
            trackBarCrMin.Value = (int)(((float)(mFilter.Crmin * scaleFactor)) + scaleFactor);
            labelCrMin.Text = "Cr Min: " + mFilter.Crmin;
            trackBarCrMax.Value = (int)(((float)(mFilter.Crmax * scaleFactor)) + scaleFactor);
            labelCrMax.Text = "Cr Max: " + mFilter.Crmax;
            trackBarYMin.Value = (int)(((float)(mFilter.Ymin * scaleFactor)));
            labelYMin.Text = "Y Min: " + mFilter.Ymin;
            trackBarYMax.Value = (int)(((float)(mFilter.Ymax * scaleFactor)));
            labelYMax.Text = "Y Max: " + mFilter.Ymax;
        }

        /// <summary>
        /// creates a bitmap from the given file path, re-sizes the image
        /// to fit in the picture box, and then displays the image in the
        /// picture box.
        /// </summary>
        /// <param name="filePath">the filepath of the bitmap to be created</param>
        private void uploadPhoto(string filePath)
        {
            // display image in picture box
            try
            {
                baseImage = new Bitmap(filePath);

                float width = STANDARD_IMAGE_HEIGHT / baseImage.Height * baseImage.Width;
                baseImage = new Bitmap(baseImage, (int)width, 350);
                pictureBox.Image = baseImage;
            }
            catch
            {
                MessageBox.Show("There was an error trying to open the image");
            }
            // display image file path in textbox
            textBoxFileName.Text = filePath;
        }

        /////////////////////////////////////////////////////////////////////////////////////
        ////////////  Background Worker Algorithm Functions/Event Handlers //////////////////
        /////////////////////////////////////////////////////////////////////////////////////

        private void ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        private void AlgorithmIsDoneRunning(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((e.Cancelled == true))
            {
                // it was cancelled
                MessageBox.Show("cancelled");
            }

            else if (!(e.Error == null))
            {
                // there was an error
                MessageBox.Show("There was an error\n\n" + e.Error.Message + "\n in \n" + e.Error.StackTrace);
            }

            else
            {
                // we finished successfully
                pictureBox.Image = filteredImage;
                MessageBox.Show("Filtered Image");
                pictureBox.Image = erodedImage;
                MessageBox.Show("Eroded Image");
                pictureBox.Image = erodedImage2;
                MessageBox.Show("Noise Elimination");
                pictureBox.Image = dilatedImage;
                MessageBox.Show("Dilation");
                pictureBox.Image = biggestBlobImage;
                MessageBox.Show("Biggest Blob");              
                pictureBox.Image = faceLocationImage;
                MessageBox.Show("X marks the spot?");
                pictureBox.Image = finalImage;
                MessageBox.Show("final image");              
                //pictureBox.Image = onlyFace;
                //MessageBox.Show("only face");
                //pictureBox.Image = inverseFace;
                //MessageBox.Show("inverse color"); 
                //pictureBox.Image = grayOnlyFace;
                //MessageBox.Show("gray face");                
                //pictureBox.Image = filteredEyeImage;
                //MessageBox.Show("eye detection");
                //pictureBox.Image = erodedEyeImage;
                //MessageBox.Show("eroded eye detection");
                //pictureBox.Image = dilatedEyeImage;
                //MessageBox.Show("dilated eye detection");
                //pictureBox.Image = narrowedEyeDetection;
                //MessageBox.Show("narrowed eye detection");
                //pictureBox.Image = finalEyeImage;
                //MessageBox.Show("did I find the eyes?");

                int version = 20;
                baseImage.Save("original-image" + version + ".png");
                filteredImage.Save("filtered-image" + version + ".png");
                erodedImage.Save("eroded-image" + version + ".png");
                erodedImage2.Save("second-eroded-image" + version + ".png");
                dilatedImage.Save("dilated-image" + version + ".png");
                biggestBlobImage.Save("biggest-blob" + version + ".png");
                faceLocationImage.Save("face-location" + version + ".png");
                finalImage.Save("final-image" + version + ".png");

                //grayOnlyFace.Save("grayscale-face" + version + ".png");
                //inverseFace.Save("inverse-face-image" + version + ".png");
                //filteredEyeImage.Save("filtered-eye-image" + version + ".png");
                //erodedEyeImage.Save("eroded-eye-image" + version + ".png");
                //dilatedEyeImage.Save("dilated-eye-image" + version + ".png");
                //narrowedEyeDetection.Save("narrowed-eye-image" + version + ".png");
                //finalEyeImage.Save("final-eye-image" + version + ".png");               
                
            }
            progressBar.Value = 0;
        }


        public void runFacialRecognitionAlgorithm(object sender, DoWorkEventArgs e)
        {
            Graphics g;
            bool borderPixel;
            int erosionDistance = 5;
            Pen bluePen = new Pen(Brushes.Blue, 2);
            Pen greenPen = new Pen(Brushes.Green, 2);
            BackgroundWorker worker = sender as BackgroundWorker;

            filteredImage = mFilter.filterBitmap(baseImage);
            erodedImage = new Bitmap(filteredImage);
            dilatedImage = new Bitmap(erodedImage);

            // calculate the mysterious 'n' value a.k.a. erosion distance, or noise elimination tolerance level.
            float whiteRatio = (float)mFilter.numberOfWhitePixels(filteredImage) / (float)(filteredImage.Height * filteredImage.Width);
            erosionDistance = (int)(whiteRatio * 40f);
            worker.ReportProgress(10);

           // EROSION /////////////////////////////////////////////////////////////////////
            for (int row = 0; row < baseImage.Height; row = row + 1)
            {
                if ((worker.CancellationPending == true))
                {
                    e.Cancel = true;
                    break;
                }
                else
                    worker.ReportProgress(row / baseImage.Height * 100);

                for (int col = 0; col < baseImage.Width; col = col + 1)
                {
                    if (filteredImage.GetPixel(col, row).ToArgb().Equals(white))
                    {
                        borderPixel = isBorderPixel(filteredImage, col, row, erosionDistance, black);
                        if (borderPixel)
                            erodedImage.SetPixel(col, row, Color.Black);
                        else
                            erodedImage.SetPixel(col, row, Color.White);
                    }
                    else
                        erodedImage.SetPixel(col, row, Color.Black);
                }
            }
            worker.ReportProgress(20);

            // erosion step 2
            erodedImage2 = new Bitmap(erodedImage);
            int toleranceLevel = 5;
            for (int iteration = 0; iteration < 3; iteration++)
            {               
                for (int row = toleranceLevel; row < baseImage.Height - toleranceLevel; row++)
                {
                    for (int col = toleranceLevel; col < baseImage.Width - toleranceLevel; col++)
                    {
                        if (erodedImage2.GetPixel(col, row).ToArgb().Equals(white))
                        {
                            if ((erodedImage2.GetPixel(col + toleranceLevel, row).ToArgb().Equals(black) // check right
                                && erodedImage2.GetPixel(col - toleranceLevel, row).ToArgb().Equals(black)) // check left
                                ||                                
                                (erodedImage2.GetPixel(col, row + toleranceLevel).ToArgb().Equals(black) // check bottom
                                && erodedImage2.GetPixel(col, row - toleranceLevel).ToArgb().Equals(black))) // check top
                               
                                erodedImage2.SetPixel(col, row, Color.Black); // black
                            else
                                erodedImage2.SetPixel(col, row, Color.White);
                        }
                    }
                }
                toleranceLevel = toleranceLevel - 2;
            }
            worker.ReportProgress(30);

            // DILATION ////////////////////////////////////////////////////////////////////
            for (int row = 0; row < baseImage.Height; row = row + 1)
            {
                if ((worker.CancellationPending == true))
                {
                    e.Cancel = true;
                    break;
                }
                else
                    worker.ReportProgress((int)(((float)row / baseImage.Height) * 40) + 30);
                for (int col = 0; col < baseImage.Width; col = col + 1)
                {
                    if (erodedImage2.GetPixel(col, row).ToArgb().Equals(black))
                    {
                        borderPixel = isBorderPixel(erodedImage2, col, row, erosionDistance, white);
                        if (borderPixel)
                            dilatedImage.SetPixel(col, row, Color.White);
                        else
                            dilatedImage.SetPixel(col, row, Color.Black);
                    }
                    else
                        dilatedImage.SetPixel(col, row, Color.White);
                }
            }
            worker.ReportProgress(70);

            // HISTOGRAM SEGMENTATION ///////////////////////////////////////////////////////////////
            int[] numberOfWhitePixelsPerRow = new int[baseImage.Height];
            int[] numberOfWhitePixelsPerColumn = new int[baseImage.Width];
            numberOfWhitePixelsPerColumn.Initialize();
            numberOfWhitePixelsPerRow.Initialize();

            for (int row = 0; row < baseImage.Height; row++)
            {
                for (int col = 0; col < baseImage.Width; col++)
                {
                    if (dilatedImage.GetPixel(col, row).ToArgb().Equals(white))
                    {
                        numberOfWhitePixelsPerColumn[col]++;
                        numberOfWhitePixelsPerRow[row]++;
                    }
                }
            }
            worker.ReportProgress(80);

            // calculate center using segmentation values (blue line)
            int largest = 0;
            Point largestPoint = new Point(0, 0);
            for (int row = 0; row < baseImage.Height; row++)
            {
                for (int col = 0; col < baseImage.Width; col++)
                {
                    int numberOfWhitePixels = numberOfWhitePixelsPerRow[row] + numberOfWhitePixelsPerColumn[col];
                    if (numberOfWhitePixels > largest)
                    {
                        largest = numberOfWhitePixels;
                        largestPoint = new Point(col, row);
                    }
                }
            }
            worker.ReportProgress(90);
            biggestBlobImage = new Bitmap(dilatedImage);
            faceDimensionsImage = new Bitmap(dilatedImage);
            faceLocationImage = new Bitmap(dilatedImage);
            finalImage = new Bitmap(baseImage);

            // use recursion to find biggest blob
            Point biggestBlobLocation = new Point(0, 0);
            int biggestBlobSize = 0;
            for (int row = 0; row < baseImage.Height; row = row + 5)
            {
                for (int col = 0; col < baseImage.Width; col = col + 5)
                {
                    if (biggestBlobImage.GetPixel(col, row).ToArgb().Equals(white))
                    {
                        int sizeOfBlob = BlobHelper.blobSize(col, row, biggestBlobImage, 5);
                        if (sizeOfBlob > biggestBlobSize)
                        {
                            biggestBlobSize = sizeOfBlob;
                            biggestBlobLocation = new Point(col, row);
                        }
                    }
                }
            }

            g = Graphics.FromImage(faceLocationImage); 
            BlobInformation mBlobInformation = new BlobInformation(dilatedImage, biggestBlobLocation);

            Point furthestPixelLeft = mBlobInformation.FurthestPixelLeft;
            Point furthestPixelRight = mBlobInformation.FurthestPixelRight;
            Point furthestPixelUp = mBlobInformation.FurthestPixelUp;
            Point furthestPixelDown = mBlobInformation.FurthestPixelDown;

            int verticalPositionOfFace = (furthestPixelDown.Y + furthestPixelUp.Y) / 2;
            int horizontalPositionOfFace = (furthestPixelLeft.X + furthestPixelRight.X) / 2;

            //g.DrawLine(bluePen, largestPoint.X, 0, largestPoint.X, baseImage.Height);
            //g.DrawLine(bluePen, 0, largestPoint.Y, baseImage.Width, largestPoint.Y);
            g.DrawLine(greenPen, horizontalPositionOfFace, 0, horizontalPositionOfFace, baseImage.Height); // vertical center
            g.DrawLine(greenPen, 0, verticalPositionOfFace, baseImage.Width, verticalPositionOfFace); // horizontal center
           
            g.FillEllipse(Brushes.Orange, furthestPixelLeft.X, furthestPixelLeft.Y, 6, 6);
            g.FillEllipse(Brushes.Orange, furthestPixelRight.X, furthestPixelRight.Y, 6, 6);
            g.FillEllipse(Brushes.Orange, furthestPixelDown.X, furthestPixelDown.Y, 6, 6);
            g.FillEllipse(Brushes.Orange, furthestPixelUp.X, furthestPixelUp.Y, 6, 6);

            g = Graphics.FromImage(finalImage);
            //g.DrawLine(bluePen, largestPoint.X, 0, largestPoint.X, baseImage.Height);
            //g.DrawLine(bluePen, 0, largestPoint.Y, baseImage.Width, largestPoint.Y);
            g.DrawLine(greenPen, horizontalPositionOfFace, 0, horizontalPositionOfFace, baseImage.Height); // vertical center
            g.DrawLine(greenPen, 0, verticalPositionOfFace, baseImage.Width, verticalPositionOfFace); // horizontal center
            g.DrawEllipse(greenPen, furthestPixelLeft.X, furthestPixelUp.Y, furthestPixelRight.X - furthestPixelLeft.X, furthestPixelDown.Y - furthestPixelUp.Y);

            worker.ReportProgress(100);



            // Eye detection //////////////////////////////////////////////////
            //Rectangle face = new Rectangle(furthestPixelLeft.X, furthestPixelUp.Y, furthestPixelRight.X - furthestPixelLeft.X, furthestPixelDown.Y - furthestPixelUp.Y);

            //onlyFace = new Bitmap(baseImage);            
            //inverseFace = InverseColors(onlyFace);
            //grayOnlyFace = GrayScale(inverseFace);
            //filteredEyeImage = new Bitmap(grayOnlyFace);

            //// eye color filter
            //for (int row = furthestPixelUp.Y; row < furthestPixelDown.Y; row++)
            //{
            //    for (int col = furthestPixelLeft.X; col < furthestPixelRight.X; col++)
            //    {
            //        Color color = grayOnlyFace.GetPixel(col, row);
            //        if (color.R > 210)
            //            filteredEyeImage.SetPixel(col, row, Color.White);
            //        else
            //            filteredEyeImage.SetPixel(col, row, Color.Black);
            //    }
            //}

            //// erosion
            //erodedEyeImage = new Bitmap(filteredEyeImage);
            //for (int row = furthestPixelUp.Y; row < furthestPixelDown.Y; row++)
            //{
            //    for (int col = furthestPixelLeft.X; col < furthestPixelRight.X; col++)
            //    {
            //        if (filteredEyeImage.GetPixel(col, row).ToArgb().Equals(white))
            //        {
            //            borderPixel = isBorderPixel(filteredEyeImage, col, row, 1, black);
            //            if (borderPixel)
            //                erodedEyeImage.SetPixel(col, row, Color.Black);
            //            else
            //                erodedEyeImage.SetPixel(col, row, Color.White);
            //        }
            //        else
            //            erodedEyeImage.SetPixel(col, row, Color.Black);
            //    }
            //}

            //// dilation
            //dilatedEyeImage = new Bitmap(erodedEyeImage);
            //for (int row = furthestPixelUp.Y; row < furthestPixelDown.Y; row++)
            //{
            //    for (int col = furthestPixelLeft.X; col < furthestPixelRight.X; col++)
            //    {
            //        if (erodedEyeImage.GetPixel(col, row).ToArgb().Equals(black))
            //        {
            //            borderPixel = isBorderPixel(erodedEyeImage, col, row, 1, white);
            //            if (borderPixel)
            //                dilatedEyeImage.SetPixel(col, row, Color.White);
            //            else
            //                dilatedEyeImage.SetPixel(col, row, Color.Black);
            //        }
            //        else
            //            dilatedEyeImage.SetPixel(col, row, Color.White);
            //    }
            //}


            //// use recursion to eliminate blobs that are too large to be an eye
            //narrowedEyeDetection = new Bitmap(dilatedEyeImage);
            //int largest_blob_size_that_can_still_be_an_eye = 100;
            //for (int row = furthestPixelUp.Y; row < furthestPixelDown.Y; row++)
            //{
            //    for (int col = furthestPixelLeft.X; col < furthestPixelRight.X; col++)
            //    {
            //        if (dilatedEyeImage.GetPixel(col, row).ToArgb().Equals(white))
            //        {
            //            Bitmap temporaryBitmapForFindingSize = new Bitmap(dilatedEyeImage);
            //            int blobSize = BlobHelper.blobSize(col, row, temporaryBitmapForFindingSize, 1);
            //            if (blobSize > largest_blob_size_that_can_still_be_an_eye)
            //                BlobHelper.colorBlob(col, row, narrowedEyeDetection, Color.White, Color.Black);

            //            temporaryBitmapForFindingSize.Dispose();
            //        }
            //    }
            //}

            //// find eye
            //finalEyeImage = new Bitmap(baseImage);
            //int top = furthestPixelUp.Y + furthestPixelUp.Y / 6;
            //int bottom = furthestPixelDown.Y - furthestPixelDown.Y / 3;
            //for (int row = top; row < bottom; row++)
            //{
            //    for (int col = furthestPixelLeft.X; col < furthestPixelRight.X; col++)
            //    {
            //        if (narrowedEyeDetection.GetPixel(col, row).ToArgb().Equals(white))
            //            finalEyeImage.SetPixel(col, row, Color.Blue);
            //    }
            //}

        }

        /// <summary>
        /// takes an image and returns a grayscaled version of the image    
        /// </summary>
        /// <param name="image">the image to be converted to grayscale</param>
        /// <returns>a new grayscale image</returns>
        private Bitmap GrayScale(Bitmap image)
        {
            Bitmap grayImage = new Bitmap(image);

            for (int row = 0; row < image.Height; row++)
            {
                for (int col = 0; col < image.Width; col++)
                {
                    Color color = image.GetPixel(col, row);
                    int total = color.R + color.G + color.B;
                    int gray = total / 3;
                    grayImage.SetPixel(col, row, Color.FromArgb(gray, gray, gray));
                }
            }

            return grayImage;
        }

        /// <summary>
        /// takes an image and returns a new image with the colors inversed
        /// </summary>
        /// <param name="image">the image to be inverted</param>
        /// <returns>a new image with inverted colors</returns>
        private Bitmap InverseColors(Bitmap image)
        {
            Bitmap inversedImage = new Bitmap(image);

            for (int row = 0; row < image.Height; row++)
            {
                for (int col = 0; col < image.Width; col++)
                {
                    Color color = image.GetPixel(col, row);
                    inversedImage.SetPixel(col, row, Color.FromArgb(255 - color.R, 255 - color.G, 255 - color.B));
                }
            }

            return inversedImage;
        }

        /// <summary>
        /// determines if the pixel at x,y is a border pixel (in the blob). A border pixel
        /// being defined as there being a color not of the parameter 'color' within 'distance' pixels
        /// of x,y
        /// </summary>
        /// <param name="image">the Bitmap to be analyzed</param>
        /// <param name="x">the x location of the pixel to be analyzed</param>
        /// <param name="y">the y location of the pixel to be analyzed</param>
        /// <param name="distance">the distance in each direction to look to determine whether a pixel is a border pixel</param>
        /// <param name="color">the color of the blob being analyzed</param>
        /// <returns>true if the pixel is a border pixel, false otherwise</returns>
        private bool isBorderPixel(Bitmap image, int x, int y, int distance, int color)
        {
            if (x >= image.Width || y >= image.Height)
                return false;

            for (int i = 1; i <= distance; i++)
            {
                if (x - i > 0)
                {
                    // check left
                    if (image.GetPixel(x - i, y).ToArgb().Equals(color))
                        return true;
                }
                if (y - i > 0)
                {
                    // check top
                    if (image.GetPixel(x, y - i).ToArgb().Equals(color))
                        return true;
                }
                if (x < image.Width - i)
                {
                    // check right
                    if (image.GetPixel(x + i, y).ToArgb().Equals(color))
                        return true;
                }
                if (y < image.Height - i)
                {
                    // check bottom
                    if (image.GetPixel(x, y + i).ToArgb().Equals(color))
                        return true;
                }
            }

            return false;
        }

    }
}
