﻿namespace face_recognition
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.labelUploadPhoto = new System.Windows.Forms.Label();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.buttonRun = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonUpload = new System.Windows.Forms.Button();
            this.trackBarCbMin = new System.Windows.Forms.TrackBar();
            this.trackBarYMin = new System.Windows.Forms.TrackBar();
            this.trackBarCrMin = new System.Windows.Forms.TrackBar();
            this.trackBarCrMax = new System.Windows.Forms.TrackBar();
            this.trackBarYMax = new System.Windows.Forms.TrackBar();
            this.trackBarCbMax = new System.Windows.Forms.TrackBar();
            this.labelCbMin = new System.Windows.Forms.Label();
            this.labelYMin = new System.Windows.Forms.Label();
            this.labelCrMin = new System.Windows.Forms.Label();
            this.labelCrMax = new System.Windows.Forms.Label();
            this.labelYMax = new System.Windows.Forms.Label();
            this.labelCbMax = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCbMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCrMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCrMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCbMax)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox.Location = new System.Drawing.Point(12, 147);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(434, 357);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(250, 113);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(196, 23);
            this.progressBar.TabIndex = 1;
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFileName.Location = new System.Drawing.Point(12, 32);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.Size = new System.Drawing.Size(434, 20);
            this.textBoxFileName.TabIndex = 2;
            this.textBoxFileName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxFileName_KeyPress);
            // 
            // labelUploadPhoto
            // 
            this.labelUploadPhoto.AutoSize = true;
            this.labelUploadPhoto.Location = new System.Drawing.Point(13, 11);
            this.labelUploadPhoto.Name = "labelUploadPhoto";
            this.labelUploadPhoto.Size = new System.Drawing.Size(73, 13);
            this.labelUploadPhoto.TabIndex = 3;
            this.labelUploadPhoto.Text = "Upload Image";
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBrowse.Location = new System.Drawing.Point(339, 58);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(107, 23);
            this.buttonBrowse.TabIndex = 4;
            this.buttonBrowse.Text = "Browse";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // buttonRun
            // 
            this.buttonRun.Location = new System.Drawing.Point(12, 113);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(107, 23);
            this.buttonRun.TabIndex = 5;
            this.buttonRun.Text = "Run";
            this.buttonRun.UseVisualStyleBackColor = true;
            this.buttonRun.Click += new System.EventHandler(this.buttonRun_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(125, 113);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(107, 23);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonUpload
            // 
            this.buttonUpload.Location = new System.Drawing.Point(12, 58);
            this.buttonUpload.Name = "buttonUpload";
            this.buttonUpload.Size = new System.Drawing.Size(107, 23);
            this.buttonUpload.TabIndex = 7;
            this.buttonUpload.Text = "Upload";
            this.buttonUpload.UseVisualStyleBackColor = true;
            this.buttonUpload.Click += new System.EventHandler(this.buttonUpload_Click);
            // 
            // trackBarCbMin
            // 
            this.trackBarCbMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.trackBarCbMin.Location = new System.Drawing.Point(12, 526);
            this.trackBarCbMin.Maximum = 40;
            this.trackBarCbMin.Name = "trackBarCbMin";
            this.trackBarCbMin.Size = new System.Drawing.Size(104, 45);
            this.trackBarCbMin.TabIndex = 8;
            this.trackBarCbMin.ValueChanged += new System.EventHandler(this.colorScrollbar_ValueChanged);
            // 
            // trackBarYMin
            // 
            this.trackBarYMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.trackBarYMin.Location = new System.Drawing.Point(342, 526);
            this.trackBarYMin.Maximum = 20;
            this.trackBarYMin.Name = "trackBarYMin";
            this.trackBarYMin.Size = new System.Drawing.Size(104, 45);
            this.trackBarYMin.TabIndex = 9;
            this.trackBarYMin.ValueChanged += new System.EventHandler(this.colorScrollbar_ValueChanged);
            // 
            // trackBarCrMin
            // 
            this.trackBarCrMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.trackBarCrMin.Location = new System.Drawing.Point(175, 526);
            this.trackBarCrMin.Maximum = 40;
            this.trackBarCrMin.Name = "trackBarCrMin";
            this.trackBarCrMin.Size = new System.Drawing.Size(104, 45);
            this.trackBarCrMin.TabIndex = 10;
            this.trackBarCrMin.ValueChanged += new System.EventHandler(this.colorScrollbar_ValueChanged);
            // 
            // trackBarCrMax
            // 
            this.trackBarCrMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.trackBarCrMax.Location = new System.Drawing.Point(175, 579);
            this.trackBarCrMax.Maximum = 40;
            this.trackBarCrMax.Name = "trackBarCrMax";
            this.trackBarCrMax.Size = new System.Drawing.Size(104, 45);
            this.trackBarCrMax.TabIndex = 13;
            this.trackBarCrMax.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.trackBarCrMax.ValueChanged += new System.EventHandler(this.colorScrollbar_ValueChanged);
            // 
            // trackBarYMax
            // 
            this.trackBarYMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.trackBarYMax.Location = new System.Drawing.Point(342, 579);
            this.trackBarYMax.Maximum = 20;
            this.trackBarYMax.Name = "trackBarYMax";
            this.trackBarYMax.Size = new System.Drawing.Size(104, 45);
            this.trackBarYMax.TabIndex = 12;
            this.trackBarYMax.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.trackBarYMax.ValueChanged += new System.EventHandler(this.colorScrollbar_ValueChanged);
            // 
            // trackBarCbMax
            // 
            this.trackBarCbMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.trackBarCbMax.Location = new System.Drawing.Point(12, 579);
            this.trackBarCbMax.Maximum = 40;
            this.trackBarCbMax.Name = "trackBarCbMax";
            this.trackBarCbMax.Size = new System.Drawing.Size(104, 45);
            this.trackBarCbMax.TabIndex = 11;
            this.trackBarCbMax.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.trackBarCbMax.ValueChanged += new System.EventHandler(this.colorScrollbar_ValueChanged);
            // 
            // labelCbMin
            // 
            this.labelCbMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCbMin.AutoSize = true;
            this.labelCbMin.Location = new System.Drawing.Point(12, 513);
            this.labelCbMin.Name = "labelCbMin";
            this.labelCbMin.Size = new System.Drawing.Size(46, 13);
            this.labelCbMin.TabIndex = 14;
            this.labelCbMin.Text = "Cb Min: ";
            // 
            // labelYMin
            // 
            this.labelYMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelYMin.AutoSize = true;
            this.labelYMin.Location = new System.Drawing.Point(339, 513);
            this.labelYMin.Name = "labelYMin";
            this.labelYMin.Size = new System.Drawing.Size(40, 13);
            this.labelYMin.TabIndex = 15;
            this.labelYMin.Text = "Y Min: ";
            // 
            // labelCrMin
            // 
            this.labelCrMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCrMin.AutoSize = true;
            this.labelCrMin.Location = new System.Drawing.Point(172, 513);
            this.labelCrMin.Name = "labelCrMin";
            this.labelCrMin.Size = new System.Drawing.Size(43, 13);
            this.labelCrMin.TabIndex = 16;
            this.labelCrMin.Text = "Cr Min: ";
            // 
            // labelCrMax
            // 
            this.labelCrMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCrMax.AutoSize = true;
            this.labelCrMax.Location = new System.Drawing.Point(172, 563);
            this.labelCrMax.Name = "labelCrMax";
            this.labelCrMax.Size = new System.Drawing.Size(46, 13);
            this.labelCrMax.TabIndex = 19;
            this.labelCrMax.Text = "Cr Max: ";
            // 
            // labelYMax
            // 
            this.labelYMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelYMax.AutoSize = true;
            this.labelYMax.Location = new System.Drawing.Point(339, 563);
            this.labelYMax.Name = "labelYMax";
            this.labelYMax.Size = new System.Drawing.Size(43, 13);
            this.labelYMax.TabIndex = 18;
            this.labelYMax.Text = "Y Max: ";
            // 
            // labelCbMax
            // 
            this.labelCbMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCbMax.AutoSize = true;
            this.labelCbMax.Location = new System.Drawing.Point(12, 563);
            this.labelCbMax.Name = "labelCbMax";
            this.labelCbMax.Size = new System.Drawing.Size(49, 13);
            this.labelCbMax.TabIndex = 17;
            this.labelCbMax.Text = "Cb Max: ";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(464, 636);
            this.Controls.Add(this.labelCrMax);
            this.Controls.Add(this.labelYMax);
            this.Controls.Add(this.labelCbMax);
            this.Controls.Add(this.labelCrMin);
            this.Controls.Add(this.labelYMin);
            this.Controls.Add(this.labelCbMin);
            this.Controls.Add(this.trackBarCrMax);
            this.Controls.Add(this.trackBarYMax);
            this.Controls.Add(this.trackBarCbMax);
            this.Controls.Add(this.trackBarCrMin);
            this.Controls.Add(this.trackBarYMin);
            this.Controls.Add(this.trackBarCbMin);
            this.Controls.Add(this.buttonUpload);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonRun);
            this.Controls.Add(this.buttonBrowse);
            this.Controls.Add(this.labelUploadPhoto);
            this.Controls.Add(this.textBoxFileName);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.pictureBox);
            this.Name = "mainForm";
            this.Text = "Facial Recognition";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCbMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCrMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCrMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarCbMax)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.Label labelUploadPhoto;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonUpload;
        private System.Windows.Forms.TrackBar trackBarCbMin;
        private System.Windows.Forms.TrackBar trackBarYMin;
        private System.Windows.Forms.TrackBar trackBarCrMin;
        private System.Windows.Forms.TrackBar trackBarCrMax;
        private System.Windows.Forms.TrackBar trackBarYMax;
        private System.Windows.Forms.TrackBar trackBarCbMax;
        private System.Windows.Forms.Label labelCbMin;
        private System.Windows.Forms.Label labelYMin;
        private System.Windows.Forms.Label labelCrMin;
        private System.Windows.Forms.Label labelCrMax;
        private System.Windows.Forms.Label labelYMax;
        private System.Windows.Forms.Label labelCbMax;
    }
}

